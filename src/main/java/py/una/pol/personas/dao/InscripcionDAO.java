package py.una.pol.personas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import py.una.pol.personas.model.Inscripcion;
import py.una.pol.personas.model.Persona;

@Stateless
public class InscripcionDAO {

    @Inject
    private Logger log;
    
	/**
	 * 
	 * @param condiciones 
	 * @return
	 */
	public List<Inscripcion> seleccionar() {
		String query = "SELECT cedula, materia FROM inscripcion";

		List<Inscripcion> lista = new ArrayList<Inscripcion>();

		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	ResultSet rs = conn.createStatement().executeQuery(query);

        	while(rs.next()) {
        		Inscripcion i = new Inscripcion();
        		i.setCedula(rs.getLong(1));
        		i.setAsignatura(rs.getString(2));
        		
        		lista.add(i);
        	}

        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}

	public List<Inscripcion> seleccionarPorCedula(long cedula) {
		String SQL = "SELECT cedula, materia FROM inscripcion WHERE cedula = ? ";

		Inscripcion i = null;
		Connection conn = null;
		List<Inscripcion> lista = new ArrayList<Inscripcion>();

		try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, cedula);

        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		i = new Inscripcion();
        		i.setCedula(rs.getLong(1));
        		i.setAsignatura(rs.getString(2));

        		lista.add(i);
        	}
        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally{
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;
	}

	public List<Inscripcion> seleccionarPorAsignatura(String asignatura) {
		String SQL = "SELECT cedula, materia FROM inscripcion WHERE materia = ? ";

		Inscripcion i = null;
		Connection conn = null;
		List<Inscripcion> lista = new ArrayList<Inscripcion>();

		try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setString(1,asignatura);

        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		i = new Inscripcion();
        		i.setCedula(rs.getLong(1));
        		i.setAsignatura(rs.getString(2));

        		lista.add(i);
        	}
        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally{
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;
	}

	public long insertar(Inscripcion i) throws SQLException {

        String SQL = "INSERT INTO inscripcion(cedula, materia)"
                + "VALUES(?,?)";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, i.getCedula());
            pstmt.setString(2, i.getAsignatura());
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }

    public long borrar(Inscripcion i) throws SQLException {

        String SQL = "DELETE FROM inscripcion WHERE cedula = ? AND materia = ? ";

        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, i.getCedula());
            pstmt.setString(2, i.getAsignatura());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	log.severe("Error en la eliminación: " + ex.getMessage());
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la eliminación: " + ex.getMessage());
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        		throw ef;
        	}
        }
        return id;
    }

    public Boolean consultar(Inscripcion i) {
		String SQL = "SELECT cedula, materia FROM inscripcion WHERE cedula = ? AND materia = ? ";

		Inscripcion r = null;
		Connection conn = null; 

		try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, i.getCedula());
        	pstmt.setString(2, i.getAsignatura());

        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		r = new Inscripcion();
        		r.setCedula(rs.getLong(1));
        		r.setAsignatura(rs.getString(2));
        	}
        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		if(r == null)
			return false;
		else
			return true;
	}
}

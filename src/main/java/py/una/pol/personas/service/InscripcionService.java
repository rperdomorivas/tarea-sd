/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.una.pol.personas.service;

import javax.ejb.Stateless;
import javax.inject.Inject;
import py.una.pol.personas.dao.InscripcionDAO;
import py.una.pol.personas.model.Inscripcion;
import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class InscripcionService {

    @Inject
    private Logger log;

    @Inject
    private InscripcionDAO dao;

    public void crear(Inscripcion i) throws Exception {
        log.info("Creando Inscripcion: " + i.getCedula() + " " + i.getAsignatura());
        try {
        	dao.insertar(i);
        }catch(Exception e) {
        	log.severe("ERROR al crear persona: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Inscripcion creada con éxito: " + i.getCedula() + " " + i.getAsignatura() );
    }

    public Boolean existeInscripcion(Inscripcion i) throws Exception {
        log.info("Consultando Inscripcion: " + i.getCedula() + " " + i.getAsignatura());
        Boolean b = false;
        try {
        	b = dao.consultar(i);
        }catch(Exception e) {
        	log.severe("ERROR al consultar inscripcion: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Inscripcion consultada con éxito: " + i.getCedula() + " " + i.getAsignatura() );
        return b;
    }

    public List<Inscripcion> seleccionar() {
    	return dao.seleccionar();
    }
    
    public List<Inscripcion> seleccionarPorCedula(long cedula) {
    	return dao.seleccionarPorCedula(cedula);
    }

    public List<Inscripcion> seleccionarPorAsignatura(String asignatura){
    	return dao.seleccionarPorAsignatura(asignatura);
    }

    public long borrar(Inscripcion i) throws Exception {
    	return dao.borrar(i);
    }
}

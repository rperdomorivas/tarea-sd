/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.una.pol.personas.service;


import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.AsignaturaDAO;
import py.una.pol.personas.model.Asignatura;

import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class AsignaturaService {

    @Inject
    private Logger log;

    @Inject
    private AsignaturaDAO dao;

    public void crear(Asignatura a) throws Exception {
        log.info("Creando Asignatura: " + a.getNombre() + " - " + a.getProfesor());
        try {
        	dao.insertar(a);
        }catch(Exception e) {
        	log.severe("ERROR al crear persona: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura creada con éxito: " + a.getNombre() );
    }

    public List<Asignatura> seleccionar() {
    	return dao.seleccionar();
    }
    
    public Asignatura seleccionarPorNombre(String nombre) {
    	return dao.seleccionarPorNombre(nombre);
    }
    
    public String borrar(String nombre) throws Exception {
    	return dao.borrar(nombre);
    }

    public void modificar(Asignatura a) throws Exception{
        log.info("Actualizando asignatura: " + a.getNombre() + " " + a.getProfesor());
        try {
        	dao.modificar(a);
        }catch(Exception e) {
        	log.severe("ERROR al actualizar asignatura: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura actualizada con éxito: " + a.getNombre() + " " + a.getProfesor() );
    }
}
